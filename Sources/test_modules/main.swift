import Extension
import Protocol


func useImplementation(p: Implementation) {
	p.hello()
}

func useExtension(p: Protocol) {
	p.hello()
}

func useExtensionGeneric<P : Protocol>(p: P) {
	p.hello()
}

let implem = Implementation()
implem.hello()
useImplementation(p: implem)
useExtension(p: implem)
useExtensionGeneric(p: implem)
useExtensionInExtension(p: implem)
useExtensionInExtensionGeneric(p: implem)

print("---")

func useImplementation2(p: Implementation2) {
	p.hello2()
}

func useExtension2(p: Protocol2) {
	p.hello2()
}

func useExtensionGeneric2<P : Protocol2>(p: P) {
	p.hello2()
}

let implem2 = Implementation2()
implem2.hello2()
useImplementation2(p: implem2)
useExtension2(p: implem2)
useExtensionGeneric2(p: implem2)
